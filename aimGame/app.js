const board = document.querySelector("#board");
const startBtn = document.querySelector(".start");
const screens = document.querySelectorAll(".screen");
const timeList = document.querySelector(".time-list");
const timerElement = document.querySelector("#time");
const aims = ["circle", "square"];
let interval;
let time = 0;
let totalScore = 0;

startBtn.addEventListener("click", (e) => {
  e.preventDefault();
  screens[0].classList.add("up");
});

timeList.addEventListener("click", (e) => {
  if (e.target.classList.contains("time-btn")) {
    screens[1].classList.add("up");
    time = +e.target.innerText.replace(
      /([а-я]+)|([А-Я]+)|([a-z]+)|([A-Z]+)/,
      ""
    );
    startGame();
  }
});

const formatedTime = (timeValue) =>
  `00:${timeValue < 10 ? "0" + timeValue.toString() : timeValue}`;

const setTime = (timeValue) => {
  timerElement.innerHTML = formatedTime(timeValue);
};

const getRandomHEXColor = () =>
  "#" +
  Math.floor(Math.random() * 2 ** 24)
    .toString(16)
    .padStart(6, 6);

//The maximum is inclusive and the minimum is inclusive
const getRandomIntInclusive = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

const createRandomAim = () => {
  const index = getRandomIntInclusive(0, 1);
  const currentAimClass = aims[index];
  const aim = document.createElement("div");
  const size = getRandomIntInclusive(10, 45);
  const aimColor = getRandomHEXColor();
  const { height, width } = board.getBoundingClientRect();

  const x = getRandomIntInclusive(0, width - size);
  const y = getRandomIntInclusive(0, height - size);

  aim.classList.add(currentAimClass);

  aim.style.width = `${size}px`;
  aim.style.height = `${size}px`;
  aim.style.top = `${x}px`;
  aim.style.left = `${y}px`;
  aim.style.background = aimColor;
  aim.style.boxShadow = `0 0 2px ${aimColor}, 0 0 10px ${aimColor}`;

  board.append(aim);
};

board.addEventListener("click", (e) => {
  if (
    e.target.classList.contains(aims[0]) ||
    e.target.classList.contains(aims[1])
  ) {
    totalScore++;
    e.target.remove();
    createRandomAim();
  }
});

const finishGame = () => {
  setTime(0);
  timerElement.parentNode.remove();
  clearInterval(interval);
  board.innerHTML = `<h1>Cчёт: <span class="totalScore">${totalScore}</span></h1>
	<br/>
	<button class="newGameBtn">Новая игра?</button>
	`;
  const newGameBtn = document.querySelector(".newGameBtn");
  newGameBtn.onclick = () => {
    totalScore = 0;
    board.innerHTML = "";
    screens[1].classList.remove("up");
    screens[2].insertAdjacentElement("afterbegin", timerElement.parentNode);
  };
};

const startGame = () => {
  interval = setInterval(countDown, 1000);
  createRandomAim();
  setTime(time);
};

const countDown = () => {
  if (time === 0) finishGame();
  else {
    let current = time--;
    setTime(current);
  }
};

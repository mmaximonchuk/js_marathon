const upBtn = document.querySelector(".up-button");
const downBtn = document.querySelector(".down-button");
const sidebar = document.querySelector(".sidebar");
const mainSlide = document.querySelector(".main-slide");
const container = document.querySelector(".container");

const slidesCount = mainSlide.querySelectorAll("div").length;
let activeSlideIndex = 0;

sidebar.style.top = `-${(slidesCount - 1) * 100}vh`;

downBtn.addEventListener("click", () => switchSlide("down"));

upBtn.addEventListener("click", () => switchSlide("up"));

const switchSlide = (direction) => {
  if (direction === "up") {
    activeSlideIndex = activeSlideIndex + 1;
    if (activeSlideIndex === slidesCount) {
      activeSlideIndex = slidesCount - 1;
    }
  } else if (direction === "down") {
    activeSlideIndex = activeSlideIndex - 1;
    if (activeSlideIndex < 0) {
      activeSlideIndex = 0;
    }
  }
  const screenHeight = container.clientHeight;

  // console.log("screenHeight: ", screenHeight);
  // console.log("activeSlideIndex: ", activeSlideIndex);

  mainSlide.style.transform = `translateY(-${
    screenHeight * activeSlideIndex
  }px)`;
  sidebar.style.transform = `translateY(${screenHeight * activeSlideIndex}px)`;
};

document.addEventListener("keypress", (e) => {
  console.log("e.key: ", e.key);
  if (e.key === "ArrowUp" || e.key === "w") {
    switchSlide("up");
  }
  if (e.key === "ArrowDown" || e.key === "s") {
    switchSlide("down");
  }
});

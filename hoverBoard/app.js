const board = document.querySelector("#board");
const SQUARES_NUMBER = 500;

for (let i = 0; i < SQUARES_NUMBER; i++) {
  const square = document.createElement("div");
  square.classList.add("square");

  square.addEventListener("mouseover", (e) => {
    handleColor(square);
  });
  square.addEventListener("mouseleave", (e) => {
    handleColor(square, true);
  });

  board.append(square);
}

const handleColor = (element, shouldRemove = false) => {
  let color = shouldRemove ? "#1d1d1d" : getRandomHEXColor();
  const currentBoxShadow = shouldRemove
    ? `0 0 2px #000`
    : `0 0 2px ${color}, 0 0 10px ${color}`;
  element.style.background = color;
  element.style.boxShadow = currentBoxShadow;
};

const getRandomHEXColor = () => {
  return (
    "#" +
    Math.floor(Math.random() * 2 ** 24)
      .toString(16)
      .padStart(6, 6)
  );
};

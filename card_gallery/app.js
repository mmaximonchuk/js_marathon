const slides = document.querySelectorAll(".slide");

slides.forEach(slide => slide.addEventListener('click', (e) => handleSlideOpen(e)));

const handleSlideOpen = (e) => {
	resetActiveSlides();
	e.target.classList.add('active');
}

const resetActiveSlides = () =>
  slides.forEach((slide) => slide.classList.remove('active'))